package pl.kamil.fractions;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * main class opening program
 * @author Mateusz Dudek
 *
 */

public class FractionMain extends Application {

    public static void main(String[] args) {
//        VulgarFraction vf1 = new VulgarFraction(1,2);
//        VulgarFraction vf2 = new VulgarFraction(1,4);
//        Actions actions = new Actions();

//        System.out.println(actions.lcm(4,4));

//        System.out.println(actions.addVulgarFraction(vf1,vf2).toString());
//        vf1 = new VulgarFraction(1,2);
//        vf2 = new VulgarFraction(1,4);
//        System.out.println(actions.subVulgarFraction(vf1,vf2).toString());
//        vf1 = new VulgarFraction(1,2);
//        vf2 = new VulgarFraction(1,4);
//        System.out.println(actions.mulVulgarFraction(vf1,vf2).toString());
//        vf1 = new VulgarFraction(1,2);
//        vf2 = new VulgarFraction(1,4);
//        System.out.println(actions.divVulgarFraction(vf1,vf2).toString());
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        AnchorPane anchorPane = FXMLLoader.load(getClass().getResource("/fxml/mainPane.fxml"));
        Scene scene = new Scene(anchorPane);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Fractions Calculator");
        primaryStage.show();
    }
}
