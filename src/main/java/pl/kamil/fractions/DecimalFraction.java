//@author kamil.trzcinski@gmail.com
package pl.kamil.fractions;

public class DecimalFraction {
    private double decimalFraction;

    public DecimalFraction(){};
    public DecimalFraction(double decimalFraction){
        this.decimalFraction = decimalFraction;
    }

    public double getDecimalFraction() {
        return decimalFraction;
    }

    public void setDecimalFraction(double decimalFraction) {
        this.decimalFraction = decimalFraction;
    }

    @Override
    public String toString() {
        return decimalFraction + "";
    }
}
