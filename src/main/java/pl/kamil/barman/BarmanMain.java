package pl.kamil.barman;

// zadanie ze strony https://javastart.pl/kurs/java-zadania

// Zdefiniuj klasę Barman, która posiada metodę createDrink przyjmującą jako argumenty nazwy oraz ilość 3 składników
// potrzebnych do przygotowania drinka. W wyniku metoda powinna zwracać obiekt typu Drink z informacjami o składnikach
// danego drinka. Każdy składnik w klasie Drink powinien być reprezentowany przez klasę Ingredient, która przechowuje
// naze oraz ilość danego składnika. Klasa Barman powinna także posiadać metodę printDrink, która przyjmuje jako
// parametr obiekt typu Drink i wyświetla o nim informację w postaci:

// Składnik drinka to sok jabłkowy, wódka, sok gruszkowy w proporcjach 0.4, 0.2, 0.4

// Dla typu danych: sok jabłkowy, 100, wódka, 50, sok gruszkowy, 100

public class BarmanMain {
    public static void main(String[] args) {
        Barman barman = new Barman();
        Ingredient ingredient1 = new Ingredient("Sok Jabłkowy", 100);
        Ingredient ingredient2 = new Ingredient("Wódka", 50);
        Ingredient ingredient3 = new Ingredient("Sok Gruszkowy", 100);

        barman.printDrink(barman.createDrink(ingredient1, ingredient2, ingredient3));
    }
}
