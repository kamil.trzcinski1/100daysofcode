package pl.kamil.barman;

public class Barman {

    public Drink createDrink(Ingredient ingredient1, Ingredient ingredient2, Ingredient ingredient3){
        Drink drink = new Drink(ingredient1, ingredient2, ingredient3);
        return drink;
    }

    public void printDrink(Drink drink){
        System.out.println(drink.toString());
    }
}
