package pl.kamil.barman;

public class Drink {
    private Ingredient[] ingredients = new Ingredient[3];
    private double portionIng1, portionIng2, portionIng3, sum;

    public Drink(Ingredient ingredient1, Ingredient ingredient2, Ingredient ingredient3) {
        ingredients[0] = ingredient1;
        ingredients[1] = ingredient2;
        ingredients[2] = ingredient3;
        sum = (ingredient1.getQuantity()+ingredient2.getQuantity()+ingredient3.getQuantity());
        portionIng1 = setPortion(ingredient1);
        portionIng2 = setPortion(ingredient2);
        portionIng3 = setPortion(ingredient3);
    }


    double setPortion(Ingredient ingredient){
        return ingredient.getQuantity()*100/sum/100;
    }

    @Override
    public String toString() {
        return "Składniki drinka to: " + ingredients[0].getIngredient() + ", " + ingredients[1].getIngredient() + ", "
                + ingredients[2].getIngredient() + " w proporcjach " + portionIng1 + ", " + portionIng2 + ", "
                + portionIng3 + ".";
    }
}
