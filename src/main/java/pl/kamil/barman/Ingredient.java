package pl.kamil.barman;

public class Ingredient {
    private String ingredient;
    private int quantity;

    public Ingredient(String ingredient, int quantity){
        this.ingredient = ingredient;
        this.quantity = quantity;
    }

    public String getIngredient() {
        return ingredient;
    }

    public int getQuantity() {
        return quantity;
    }
}
